import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

public class FedosL4Main {
    public static void main(String[] args) {
        String is="input.txt",os="output.txt";
        Graph g = new Graph();
        g.readGraph(is);

        g.transformToListOfEdges();

        BufferedWriter writer = null;
        try
        {
            File file = new File(os);
            writer = new BufferedWriter(new FileWriter(file));

            String mes;
            if( g.checkBipart() == false )
            {
                mes = String.format("%1$d", -1).trim();
                writer.write(mes);
                writer.newLine();
            }
            else
            {
                ArrayList<Edge> vec = g.getMaximumMatchingBipart();
                mes = String.format("%1$d", vec.size()).trim();
                writer.write(mes);
                writer.newLine();

                for ( Edge i : vec)
                {
                    writer.write(i.toString());
                    writer.newLine();
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
//noinspection ConstantConditions
                writer.close();
            } catch (Exception ignored) {}
        }
    }
}
