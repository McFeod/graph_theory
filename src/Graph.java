import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.*;
import java.util.*;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.*;
/*****************************************************************************/
/** AdjListGraph.java **/
/*****************************************************************************/

/**
 * Список смежных вершин на основе хеш-таблиц
 */
class AdjListGraph extends ConvertableGraph{
    private ArrayList<HashMap<Integer, Integer>> representation;

    public AdjListGraph(int vertexCount, boolean oriented, boolean weighted) {
        super(vertexCount, oriented, weighted);
        setTypeLiteral('L');
        representation = new ArrayList<>();
        for(int i=0; i<vertexCount; ++i){
            representation.add(i, new HashMap<>());
        }
    }

    @Override
    public void addEdge(int from, int to, int weight) {
        super.addEdge(from, to, weight);
        representation.get(from).put(to, weight);
    }

    @Override
    public void removeEdge(int from, int to) {
        super.removeEdge(from, to);
        representation.get(from).remove(to);
    }

    @Override
    public int changeEdge(int from, int to, int newWeight) {
        int old = representation.get(from).get(to);
        representation.get(from).put(to, newWeight);
        return old;
    }

    public void feedEdge(int from, int to, int deltaWeight){
        Integer old = representation.get(from).get(to);
        if (old == null) addEdge(from, to, deltaWeight);
        else changeEdge(from, to, old + deltaWeight);
    }

    public void dryEdge(int from, int to, int deltaWeight){
        Integer old = representation.get(from).get(to);
        if (old > deltaWeight) changeEdge(from, to, old - deltaWeight);
        else removeEdge(from, to);
    }

    @Override
    public AdjListGraph toAdjListGraph() {
        return this;
    }

    @Override
    protected void copyData(GraphInterface newGraph){
        for(int i = 0; i< representation.size(); ++i){
            for(Map.Entry entry: representation.get(i).entrySet()){
                newGraph.addEdge(i, (Integer)entry.getKey(), (Integer)entry.getValue());
            }
        }
    }

    @Override
    protected void writeRepresentation(FileWriter writer) throws IOException {
        if (isWeighted()){
            for (Map<Integer, Integer> map: representation) writer.write(mapToString(map));
        } else {
            for (Map<Integer, Integer> map: representation) writer.write(mapKeysToString(map));
        }
    }

    /**
     * Одна строка в представлении невзвешенного графа
     */
    private static String mapKeysToString(Map<Integer, Integer> map) {
        StringBuilder builder = new StringBuilder();
        for (int key: map.keySet()){
            builder.append(key+1).append(" ");
        }
        if (builder.length() == 0)
            return "\n";
        builder.setCharAt(builder.length()-1, '\n');
        return builder.toString();
    }

    /**
     * Одна строка в представлении взвешенного графа
     */
    private static String mapToString(Map<Integer, Integer> map) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<Integer, Integer> entry: map.entrySet()){
            builder.append(entry.getKey()+1).append(" ").append(entry.getValue()).append(" ");
        }
        if (builder.length() == 0)
            return "\n";
        builder.setCharAt(builder.length()-1, '\n');
        return builder.toString();
    }

    @Override
    public ArrayList<HashMap<Integer, Integer>> getRepresentation() {
        return representation;
    }

    public Set<Integer> getNeighbours(int vertex){
         return representation.get(vertex).keySet();
    }

    public AdjListGraph(AdjListGraph another){
        super(another.getVertexCount(), another.isOriented(), another.isWeighted());
        setTypeLiteral('L');
        for (int i=0; i<another.getVertexCount(); ++i){
            outDegrees[i] = another.outDegrees[i];
        }
        if (isOriented()){
            for (int i=0; i<another.getVertexCount(); ++i){
                inDegrees[i] = another.inDegrees[i];
            }
        }
        ArrayList<HashMap<Integer, Integer>> anotherRepresentation = another.getRepresentation();
        representation = new ArrayList<>(another.getVertexCount());
        for(int i=0; i<another.getVertexCount(); ++i){
            representation.add(i, new HashMap<>(anotherRepresentation.get(i)));
        }
    }

}
/*****************************************************************************/
/** AdjMatrixGraph.java **/
/*****************************************************************************/
/**
 * Матрица смежности на основе массива
 */
class AdjMatrixGraph extends ConvertableGraph{
    private ArrayList<ArrayList<Integer>> representation;

    public AdjMatrixGraph(int vertexCount, boolean oriented, boolean weighted) {
        super(vertexCount, oriented, weighted);
        setTypeLiteral('C');
        representation = new ArrayList<>();
        for (int i=0; i<vertexCount; ++i){
            representation.add(new ArrayList<>(Collections.nCopies(vertexCount, 0)));
        }
    }

    @Override
    public void addEdge(int from, int to, int weight) {
        super.addEdge(from, to, weight);
        representation.get(from).set(to, weight);
    }

    @Override
    public void removeEdge(int from, int to) {
        super.removeEdge(from, to);
        addEdge(from, to, 0);
    }

    @Override
    public int changeEdge(int from, int to, int newWeight) {
        int old = representation.get(from).get(to);
        representation.get(from).set(to, newWeight);
        return old;
    }

    @Override
    protected void copyData(GraphInterface newGraph) {
        for (int i = 0; i< representation.size(); ++i){
            for(int j = 0; j< representation.get(i).size(); ++j){
                if (representation.get(i).get(j) != 0)
                    newGraph.addEdge(i, j, representation.get(i).get(j));
            }
        }
    }

    @Override
    public AdjMatrixGraph toAdjMatrixGraph() {
        return this;
    }

    @Override
    protected void writeRepresentation(FileWriter writer) throws IOException {
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<getVertexCount(); ++i){
            for(int j=0; j<getVertexCount(); ++j){
                builder.append(representation.get(i).get(j)).append(" ");
            }
            builder.setCharAt(builder.length()-1, '\n');
            writer.write(builder.toString());
            builder.setLength(0);
        }
    }

    @Override
    public ArrayList<ArrayList<Integer>> getRepresentation() {
        return representation;
    }
}
/*****************************************************************************/
/** BipartHelper.java **/
/*****************************************************************************/

class BipartHelper {
    private static Integer graphHash;
    private static AdjListGraph graph;
    private static boolean resultOfCheck;
    private static ArrayList<Integer> bipartMarks;
    private static ArrayList<Integer> matching;
    private static ArrayList<Boolean> kuhnVisited;



    private static void init(ConvertableGraph graph){
        if ((graphHash == null)||(graph.hashCode() != graphHash)) {
            BipartHelper.graphHash = graph.hashCode();
            BipartHelper.graph = graph.toAdjListGraph();
            bipartMarks = new ArrayList<>(
                    Collections.nCopies(BipartHelper.graph.getVertexCount(), -1));
            resultOfCheck = checkBipart();
        }
    }

    private static boolean checkBipart(){
        LinkedList<Edge> bfsQueue = new LinkedList<>();
        for(int i=0; i<bipartMarks.size(); ++i){
            if (bipartMarks.get(i) == -1){
                pushNeighbours(bfsQueue, i, 1);
                Edge temp;
                int mark;
                while(!bfsQueue.isEmpty()){
                    temp = bfsQueue.poll();
                    mark = (temp.getEnd() + 1) % 2;
                    if (bipartMarks.get(temp.getStart()) == -1){
                        bipartMarks.set(temp.getStart(), mark);
                        pushNeighbours(bfsQueue, temp.getStart(), mark);
                    }
                    else if (bipartMarks.get(temp.getStart()) == temp.getEnd()){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private static void pushNeighbours(LinkedList<Edge> collection, int vertex, int mark){
        for (int i: BipartHelper.graph.getNeighbours(vertex)){
            collection.add(new Edge(i, mark));
        }
    }

    public static boolean isBipart(ConvertableGraph graph){
        init(graph);
        return resultOfCheck;
    }


    public static ArrayList<Edge> kuhn(ConvertableGraph graph){
        if (!isBipart(graph)) return null;
        kuhnSolver();
        ArrayList<Edge> result = new ArrayList<>();
        for(int i=0; i<matching.size(); i++){
            int match = matching.get(i);
            if (match != -1 && match > i)
                result.add(new Edge(i, matching.get(i)));
        }
        return result;
    }

    private static void kuhnSolver(){
        matching = new ArrayList<>(Collections.nCopies(graph.getVertexCount(), -1));
        kuhnVisited = new ArrayList<>(Collections.nCopies(graph.getVertexCount(), false));

        for (int i=0; i<bipartMarks.size(); ++i){
            if (bipartMarks.get(i) == 0){
                kuhnStep(i);
            }
        }
    }

    private static boolean kuhnStep(int vertex){
        if (kuhnVisited.get(vertex)) return false;
        kuhnVisited.set(vertex, true);
        for(int v: graph.getNeighbours(vertex)){
            int match = matching.get(v);
            if (match == -1){
                matching.set(v, vertex);
                matching.set(vertex, v);
                return true;
            }
        }
        for(int v: graph.getNeighbours(vertex)){
            int match = matching.get(v);
            if (kuhnStep(match)){
                matching.set(vertex, v);
                matching.set(v, vertex);
                return true;
            }
        }
        return false;
    }

}
/*****************************************************************************/
/** Boruvka.java **/
/*****************************************************************************/
class Boruvka extends MSTAlgorithm {
    private DSU dsu;
    private ListOfEdgesGraph result;
    private int vertexCount;
    private LinkedList<Map.Entry<Edge, Integer>> edges;
    private boolean isMin;

    public Boruvka(ListOfEdgesGraph graph) {
        super(graph);
        isMin = true;
        edges = new LinkedList<>();
        vertexCount = graph.getVertexCount();
        for (Map.Entry<Edge, Integer> e: graph.getRepresentation().entrySet()){
            edges.add(e);
        }
        result = new ListOfEdgesGraph(vertexCount, false, true);
        dsu = new DSU(vertexCount);
    }

    @Override
    public ConvertableGraph solve() {
        while (dsu.hasMultipleComponents() && ! edges.isEmpty()){
            ArrayList<Map.Entry<Edge, Integer>> best = new ArrayList<>(Collections.nCopies(graph.getVertexCount(), null));
            Iterator it = edges.iterator();
            Map.Entry<Edge, Integer> edge;
            while (it.hasNext()){
                edge = (Map.Entry<Edge, Integer>)it.next();
                if (dsu.inSameComponent(edge.getKey().getStart(), edge.getKey().getEnd())) {
                    it.remove();
                    continue;
                }
                int component = dsu.find(edge.getKey().getStart());
                if (best.get(component) == null || better(edge, best.get(component)))
                    best.set(component, edge);
            }
            for (Map.Entry<Edge, Integer> temp: best){
                if (temp != null){
                    result.getRepresentation().put(temp.getKey(), temp.getValue());
                    result.addEdge(temp.getKey().getEnd(), temp.getKey().getStart(), temp.getValue());
                    dsu.unite(temp.getKey().getStart(), temp.getKey().getEnd());
                }
            }
        }
        return result;
    }

    boolean better(Map.Entry<Edge, Integer> first, Map.Entry<Edge, Integer> second){
        return first.getValue() < second.getValue() == isMin;
    }
}

/*****************************************************************************/
/** ConvertableGraph.java **/
/*****************************************************************************/
/**
 * Общие для всех представлений преобразования
 */
abstract class ConvertableGraph implements GraphInterface {
    private int vertexCount;
    protected int[] outDegrees;
    protected int[] inDegrees;

    public boolean isOriented() {
        return isOriented;
    }

    public boolean isWeighted() {
        return isWeighted;
    }

    private boolean isOriented;
    private boolean isWeighted;
    private char typeLiteral;

    public char getTypeLiteral() {
        return typeLiteral;
    }

    public void setTypeLiteral(char typeLiteral) {
        this.typeLiteral = typeLiteral;
    }

    ConvertableGraph(int vertexCount, boolean oriented, boolean weighted){
        this.vertexCount = vertexCount;
        this.isOriented = oriented;
        this.isWeighted = weighted;
        this.outDegrees = new int[vertexCount];
        if (isOriented)
            this.inDegrees = new int[vertexCount];
    }

    abstract protected void copyData(GraphInterface newGraph);

    // как минимум 1 из этих методов должен быть переопределён в return this
    public AdjMatrixGraph toAdjMatrixGraph(){
        AdjMatrixGraph newGraph = new AdjMatrixGraph(vertexCount, isOriented, isWeighted);
        copyData(newGraph);
        return newGraph;
    }

    public ListOfEdgesGraph toListOfEdgesGraph(){
        ListOfEdgesGraph newGraph = new ListOfEdgesGraph(vertexCount, isOriented, isWeighted);
        copyData(newGraph);
        return newGraph;
    }

    public AdjListGraph toAdjListGraph(){
        AdjListGraph newGraph = new AdjListGraph(vertexCount, isOriented, isWeighted);
        copyData(newGraph);
        return newGraph;
    }

    abstract protected void writeRepresentation(FileWriter writer) throws IOException;

    protected void writeHeader(FileWriter writer) throws IOException{
        writer.write(typeLiteral + " " + vertexCount + "\n");
        writer.write((isOriented ? "1" : "0") + " " + (isWeighted ? "1": "0") + "\n");
    }

    @Override
    public void addEdge(int from, int to, int weight) {
        this.outDegrees[from]++;
        if (isOriented)
            this.inDegrees[to]++;
    }

    @Override
    public void removeEdge(int from, int to) {
        this.outDegrees[from]--;
        if (isOriented)
            this.inDegrees[to]--;
    }

    @Override
    public void writeGraph(String file) {
        try {
            FileWriter writer = new FileWriter(file, false);
            writeHeader(writer);
            writeRepresentation(writer);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getVertexCount() {
        return vertexCount;
    }

    public abstract Object getRepresentation();

    public Integer getDegree(Integer vertex){
        if (vertex == null)
            return null;
        if (isOriented)
            return inDegrees[vertex] + outDegrees[vertex];
        return outDegrees[vertex];
    }

    public int getOutDegree(int vertex){
        return outDegrees[vertex];
    }

    public int getInDegree(int vertex){
        if (isOriented)
            return inDegrees[vertex];
        return outDegrees[vertex];
    }

    @Override
    public int hashCode() {
        return getRepresentation().hashCode();
    }
}
/*****************************************************************************/
/** DSU.java **/
/*****************************************************************************/
class DSU {
    protected int[] parents;
    private int[] treeHeight;
    protected int components;
    DSU(int capacity){
        components = capacity;
        parents = new int[capacity];
        treeHeight = new int[capacity];
        for (int i=0; i<capacity; ++i){
            parents[i] = i;
            treeHeight[i] = 1;
        }
    }

    int find(int vertex){
        if (parents[vertex] == vertex)
            return vertex;
        else
            return parents[vertex] = find(parents[vertex]);
    }

    void unite(int first, int second){
        if (first == second) return;
        int firstRoot = find(first);
        int secondRoot = find(second);
        if (firstRoot == secondRoot) return;
        components--;
        int delta = treeHeight[firstRoot] - treeHeight[secondRoot];
        if (delta >= 0){
            parents[secondRoot] = firstRoot;
            if (delta == 0){
                treeHeight[firstRoot] += 1;
            }
        }
        else {
            parents[firstRoot] = secondRoot;
        }
    }

    public boolean inSameComponent(int first, int second){
        return (find(first) == find(second));
    }

    public boolean hasMultipleComponents(){
        return components > 1;
    }
}
/*****************************************************************************/
/** Edge.java **/
/*****************************************************************************/
class Edge implements Comparable{
    private int start;
    private int end;

    public Edge(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    @Override
    public int compareTo(Object o) {
        Edge another = (Edge) o;
        int tmp = this.start - another.start;
        if (tmp == 0){
            return this.end - another.end;
        }
        return tmp;
    }

    @Override
    public int hashCode() {
        return 31*this.start + this.end;
    }

    @Override
    public boolean equals(Object o) {
        try {
            Edge another = (Edge) o;
            return ((another.start == start) && (another.end == end));
        } catch (ClassCastException e){
            return false;
        }
    }

    @Override
    public String toString() {
        return String.valueOf(start+1) + " " + String.valueOf(end+1);
    }
}
/*****************************************************************************/
/** EulerDSU.java **/
/*****************************************************************************/
class EulerDSU extends DSU {
    private int capacity;
    EulerDSU(AdjListGraph graph) {
        super(graph.getVertexCount());
        this.capacity = graph.getVertexCount();
        ArrayList<HashMap<Integer, Integer>> repr = graph.getRepresentation();
        for(int start=0; start < capacity; ++start){
            for(int end: repr.get(start).keySet()){
                unite(start, end);
            }
        }
    }

    public boolean hasMultipleEdgeComponents(){
        Integer multiEdge = null;
        if (this.components > 1){
            for (int i=0; i < capacity; ++i){
                if (parents[i] != i) {
                    if (multiEdge == null)
                        multiEdge = parents[i];
                    else if (multiEdge != parents[i])
                        return true;
                }
            }
        }
        return false;
    }
}
/*****************************************************************************/
/** EulerHelper.java **/
/*****************************************************************************/
class EulerHelper {
    private static AdjListGraph graph;
    private static EulerDSU dsu;
    private static Integer graphHash;
    private static Integer resultOfCheck;

    private static void init(ConvertableGraph graph){
        if ((graphHash == null)||(graph.hashCode() != graphHash)) {
            EulerHelper.graphHash = graph.hashCode();
            EulerHelper.graph = graph.toAdjListGraph();
            EulerHelper.dsu = new EulerDSU(EulerHelper.graph);
            EulerHelper.resultOfCheck = EulerHelper.checkEulerRaw();
        }
    }

    public static int checkEuler(ConvertableGraph graph){
        EulerHelper.init(graph);
        if (resultOfCheck == null) {
            return 0;
        }
        return Math.abs(resultOfCheck);
    }

    public static boolean hasCylce(ConvertableGraph graph){
        EulerHelper.init(graph);
        if (resultOfCheck == null) {
            return false;
        }
        return resultOfCheck > 0;
    }

    /**
     * Проверка на наличие эйлерова цикла/пути
     * @return 0 - цикла/пути нет
     *         > 0 - вершина, с которой можно начинать цикл
     *         < 0 - -вершина, с которой можно начинать путь
     */
    public static Integer checkEulerRaw(){
        if (dsu.hasMultipleEdgeComponents())
            return 0;
        int counters[] = new int[2];
        Integer start = null;
        Integer current = graph.getDegree(0);
        for (int i=0; i<graph.getVertexCount(); counters[current=graph.getDegree(i++) % 2]++){
            if (better(graph.getDegree(start), current)){
                start = i;
            }
        }
        if (isNil(graph.getDegree(start))){
            return null;
        }
        if (counters[1] == 0)
            return start+1;
        if (counters[1] == 2)
            return -(start+1);
        return null;
    }

    private static boolean better(Integer firstDegree, Integer secondDegree){
        if (!isNil(firstDegree)){
            if ((isNil(secondDegree)) || (secondDegree%2 < firstDegree%2)){
                return false;
            }
        }
        return true;
    }

    private static boolean isNil(Integer number){
        return (number == null) || (number == 0);
    }

    public static ArrayList<Integer> getEuleranTourFleri(ConvertableGraph graph){
        EulerHelper.init(graph);
        if (resultOfCheck == null) return null;
        ArrayList<Integer> result = new ArrayList<>();
        int currentVertex, startVertex, tmp;
        currentVertex = startVertex = Math.abs(resultOfCheck) - 1;
        result.add(startVertex + 1);
        AdjListGraph workingCopy = new AdjListGraph(EulerHelper.graph);
        do{
            tmp = getFleriNeighbour(workingCopy, currentVertex);
            workingCopy.removeEdge(currentVertex, tmp);
            if (!workingCopy.isOriented()) workingCopy.removeEdge(tmp, currentVertex);
            currentVertex = tmp;
            result.add(currentVertex + 1);
        }
        while (currentVertex != startVertex);
        return result;
    }


    private static int getFleriNeighbour(AdjListGraph graph, int vertex){
        Set<Integer> neighbours = graph.getNeighbours(vertex);
        for(Integer neighbour: neighbours){
            ArrayList<Integer> visited = new ArrayList<>(Collections.nCopies(graph.getVertexCount(), -1));
            LinkedList<Integer> bfsQueue = new LinkedList<>();
            visited.set(vertex, 0);
            visited.set(neighbour, 1);
            int level = 1;
            int cursor;
            bfsQueue.add(neighbour);
            boolean isBridge = true;
            do{
                cursor = bfsQueue.poll();
                ++level;
                for (int n: graph.getNeighbours(cursor)){
                    if (visited.get(n) == -1){
                        bfsQueue.add(n);
                        visited.set(n, level);
                    }
                    else if (visited.get(n) == 0){
                        if (!graph.isOriented() && level == 2) {
                            continue;
                        }
                        isBridge = false;
                        break;
                    }
                }
            } while (isBridge && !bfsQueue.isEmpty());
            if (!isBridge) return neighbour;
        }
        return neighbours.iterator().next();
    }

    public static ArrayList<Integer> getEuleranTourEffective(ConvertableGraph graph){
        EulerHelper.init(graph);
        if (resultOfCheck == null) return null;
        ArrayList<Integer> result = new ArrayList<>();
        LinkedList<Integer> pathStack = new LinkedList<>();
        HashMap<Edge, Boolean> markers = new HashMap<>();
        int cursor = Math.abs(resultOfCheck);
        boolean deadlock;
        pathStack.add(cursor);
        while (!pathStack.isEmpty()){
            cursor = pathStack.peekLast();
            deadlock = true;
            for(int i: EulerHelper.graph.getNeighbours(cursor)){
                if (markers.get(new Edge(cursor, i)) == null){
                    deadlock = false;
                    markers.put(new Edge(cursor, i), true);
                    if (!EulerHelper.graph.isOriented()) {
                        markers.put(new Edge(i, cursor), true);
                    }
                    pathStack.add(i);
                    break;
                }
            }
            if (deadlock){
                result.add(pathStack.pollLast() + 1);
            }
        }
        return result;
    }
}
/*****************************************************************************/
/** FlowHelper.java **/
/*****************************************************************************/
class FlowHelper {
    private static LinkedList<Map.Entry<Integer, Integer>> dfsWay = new LinkedList<>();
    private static AdjListGraph workingCopy;
    private static int minimalWeight;
    private static ArrayList<Integer> layerMarks;

    public static AdjListGraph ﬂowFordFulkerson(ConvertableGraph graph, int sourse, int sink){
        if (!isNet(graph)) return null;
        AdjListGraph resultFlow = new AdjListGraph(graph.getVertexCount(), true, true);
        workingCopy = new AdjListGraph(graph.toAdjListGraph());
        boolean found;
        int cursor;
        Condition checker = new DummyCondition();
        Map.Entry<Integer, Integer> temp;
        do {
            found = false;
            cursor = sourse;
            minimalWeight = Integer.MAX_VALUE;
            ArrayList<Boolean> visited = new ArrayList<>(Collections.nCopies(workingCopy.getVertexCount(), false));
            found = dfs(sourse, sink, visited, checker);
            while (!dfsWay.isEmpty()){
                temp = dfsWay.poll();
//                System.out.println((cursor+1) + " "+  (temp.getKey()+1) + " " + minimalWeight);
                workingCopy.dryEdge(cursor, temp.getKey(), minimalWeight);
                workingCopy.feedEdge(temp.getKey(), cursor, minimalWeight);
                resultFlow.feedEdge(cursor, temp.getKey(), minimalWeight);
                cursor = temp.getKey();
            }
        } while (found);
        workingCopy = null;
        return resultFlow;
    }

    private static boolean dfs(int source, int sink, ArrayList<Boolean> visited, Condition checker){
        visited.set(source, true);
        if (source == sink) return true;
        for (Map.Entry<Integer, Integer> edge: workingCopy.getRepresentation().get(source).entrySet()){
            if (visited.get(edge.getKey())) continue;
            if (checker.check(source, edge.getKey())){
                if (dfs(edge.getKey(), sink, visited, checker)){
                    dfsWay.push(edge);
                    if (minimalWeight > edge.getValue()){
                        minimalWeight = edge.getValue();
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isNet(ConvertableGraph graph){
        return graph.isOriented() && graph.isWeighted();
    }

    private static interface Condition{
        boolean check(int first, int second);
    }
    private static class DummyCondition implements Condition{
        @Override
        public boolean check(int first, int second) {
            return true;
        }
    }

    private static class LayerCondition implements Condition{
        @Override
        public boolean check(int first, int second) {
            return layerMarks.get(first)+1 == layerMarks.get(second);
        }
    }

    private static boolean bfs(int source, int sink){
        LinkedList<Integer> queue = new LinkedList<>();
        for (int i = 0; i< layerMarks.size(); ++i){
            layerMarks.set(i, -1);
        }
        int cursor, layer;
        queue.push(source);
        layerMarks.set(source, 0);
        do{
            cursor = queue.poll();
            layer = layerMarks.get(cursor) + 1;
            for (int vertex: workingCopy.getNeighbours(cursor)){
                if (layerMarks.get(vertex) == -1){
                    layerMarks.set(vertex, layer);
                    queue.push(vertex);
                }
            }
        } while (!queue.isEmpty());
        return layerMarks.get(sink) != -1;
    }

    public static AdjListGraph ﬂowDinitz(ConvertableGraph graph, int sourse, int sink){
        if (!isNet(graph)) return null;
        AdjListGraph resultFlow = new AdjListGraph(graph.getVertexCount(), true, true);
        workingCopy = new AdjListGraph(graph.toAdjListGraph());
        layerMarks = new ArrayList<>(Collections.nCopies(workingCopy.getVertexCount(), -1));
        int cursor;
        Condition checker = new DummyCondition();
        Map.Entry<Integer, Integer> temp;
        while (bfs(sourse, sink)){
            boolean found = false;
            do {
                cursor = sourse;
                minimalWeight = Integer.MAX_VALUE;
                ArrayList<Boolean> visited = new ArrayList<>(Collections.nCopies(workingCopy.getVertexCount(), false));
                found = dfs(sourse, sink, visited, checker);
                while (!dfsWay.isEmpty()){
                    temp = dfsWay.poll();
                    workingCopy.dryEdge(cursor, temp.getKey(), minimalWeight);
                    workingCopy.feedEdge(temp.getKey(), cursor, minimalWeight);
                    resultFlow.feedEdge(cursor, temp.getKey(), minimalWeight);
                    cursor = temp.getKey();
                }
            } while (found);
        }
        workingCopy = null;
        layerMarks = null;
        return resultFlow;
    }
}
/*****************************************************************************/
/** GodGraphInterface.java **/
/*****************************************************************************/
interface GodGraphInterface extends GraphInterface{
    void readGraph(String file);
    void transformToAdjList();
    void transformToAdjMatrix();
    void transformToListOfEdges();
    int checkEuler();
    ArrayList<Integer> getEuleranTourFleri();
    ArrayList<Integer> getEuleranTourEffective();
    boolean hasCycle();
    boolean checkBipart();
    boolean checkBipart(ArrayList<Integer> marks);
    ArrayList<Edge> getMaximumMatchingBipart();
    GodGraphInterface getSpanningTreePrima();
    GodGraphInterface getSpanningTreeKruscal();
    GodGraphInterface getSpanningTreeBoruvka();
    GodGraphInterface ﬂowFordFulkerson(int source, int sink);
    GodGraphInterface ﬂowDinitz(int source, int sink);
}

/*****************************************************************************/
/** GraphFactory.java **/
/*****************************************************************************/
/**
 * Чтение и парсинг входного файла, с последующим созданием графа нужного вида
 */
class GraphFactory {
    BufferedReader reader;
    public GraphFactory(String name) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(name));
    }

    public ConvertableGraph getGraph() throws NumberFormatException, IOException, WrongInputFormatException{
        ConvertableGraph graph;
        InputStrategy strategy;  // обработка нужного представления
        int linesToRead;
        String[] mainSettings = reader.readLine().split(" ");  // читаем заголовок
        int vertexCount = Integer.valueOf(mainSettings[1]);
        String[] extraSettings = reader.readLine().split(" "); // и "подзаголовок"
        boolean isOriented = Objects.equals(extraSettings[0], "1");
        boolean isWeighted = Objects.equals(extraSettings[1], "1");
        // создаём граф нужного типа и соответствующую стратегию обработки представления
        switch (mainSettings[0]) {
            case "C":
                linesToRead = vertexCount;
                strategy = new AdjMatrixStrategy();
                graph = new AdjMatrixGraph(vertexCount, isOriented, isWeighted);
                break;
            case "E":
                linesToRead = Integer.valueOf(mainSettings[2]);
                graph = new ListOfEdgesGraph(vertexCount, isOriented, isWeighted);
                strategy = isWeighted ? new ListOfEdgesWeightedStrategy(): new ListOfEdgesSimpleStrategy();
                break;
            case "L":
                linesToRead = vertexCount;
                graph = new AdjListGraph(vertexCount, isOriented, isWeighted);
                strategy = isWeighted? new AdjListWeightedStrategy(): new AdjListSimpleStrategy();
                break;
            default: throw new WrongInputFormatException();
        }
        // заполняем граф содержимым
        for (int i=0; i<linesToRead; ++i){
            strategy.parseString(reader.readLine(), graph, i);
        }
        return graph;
    }
}
/*****************************************************************************/
/** GraphInterface.java **/
/*****************************************************************************/
interface GraphInterface {
    void addEdge(int from, int to, int weight);
    void removeEdge(int from, int to);
    int  changeEdge(int from, int to, int newWeight);
    void writeGraph(String file);
}
/*****************************************************************************/
/** Graph.java **/
/*****************************************************************************/
class Graph implements GodGraphInterface {
    private ConvertableGraph graph;  // собственно, сам граф в некотором представлении

    @Override
    public void readGraph(String file) {
        try {
            graph = new GraphFactory(file).getGraph();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    // В "божественном" графе нумерация вершин начинается с 1
    @Override
    public void addEdge(int from, int to, int weight) {
        graph.addEdge(from-1, to-1, weight);
        /* здесь и далее: неориентированный граф преставляем в пямяти так же,
         * как и ориентированный. */
        if (!graph.isOriented() && (from != to)){
            graph.addEdge(to-1, from-1, weight);
        }
    }

    @Override
    public void removeEdge(int from, int to) {
        graph.removeEdge(from-1, to-1);
        if (!graph.isOriented() && (from != to)){
            graph.removeEdge(to-1, from-1);
        }
    }

    @Override
    public int changeEdge(int from, int to, int newWeight) {
        int result = graph.changeEdge(from-1, to-1, newWeight);
        if (!graph.isOriented() && (from != to)){
            graph.changeEdge(to-1, from-1, newWeight);
        }
        return result;
    }

    @Override
    public void transformToAdjList() {
        graph = graph.toAdjListGraph();
    }

    @Override
    public void transformToAdjMatrix() {
        graph = graph.toAdjMatrixGraph();
    }

    @Override
    public void transformToListOfEdges() {
        graph = graph.toListOfEdgesGraph();
    }

    @Override
    public void writeGraph(String file) {
        graph.writeGraph(file);
    }

    @Override
    public Graph getSpanningTreeBoruvka() {
        this.transformToListOfEdges();
        return new Graph(new Boruvka((ListOfEdgesGraph) this.graph).solve());
    }

    @Override
    public Graph getSpanningTreeKruscal() {
        this.transformToListOfEdges();
        return new Graph(new Kruscal((ListOfEdgesGraph) this.graph).solve());
    }

    @Override
    public Graph getSpanningTreePrima() {
        this.transformToAdjList();
        return new Graph(new Prim((AdjListGraph) this.graph).solve());
    }

    // Зачем, если всё равно читать из файла? Тем не менее, всё по спецификации
    Graph(int vertexCount){}
    Graph(){}
    Graph(ConvertableGraph graph){
        this.graph = graph;
    }

    @Override
    public boolean hasCycle() {
        return EulerHelper.hasCylce(graph);
    }

    @Override
    public int checkEuler() {
        return EulerHelper.checkEuler(graph);
    }

    @Override
    public ArrayList<Integer> getEuleranTourFleri() {
        return EulerHelper.getEuleranTourFleri(graph);
    }

    @Override
    public ArrayList<Integer> getEuleranTourEffective() {
        return EulerHelper.getEuleranTourEffective(graph);
    }

    @Override
    public boolean checkBipart() {
        return BipartHelper.isBipart(graph);
    }

    // только ради спецификации
    @Override
    public boolean checkBipart(ArrayList<Integer> marks) {
        return this.checkBipart();
    }

    @Override
    public ArrayList<Edge> getMaximumMatchingBipart() {
        return BipartHelper.kuhn(graph);
    }

    @Override
    public Graph ﬂowFordFulkerson(int source, int sink) {
        return new Graph(FlowHelper.ﬂowFordFulkerson(graph, source-1, sink-1));
    }

    @Override
    public Graph ﬂowDinitz(int source, int sink) {
        return new Graph(FlowHelper.ﬂowDinitz(graph, source-1, sink-1));
    }
}
/*****************************************************************************/
/** InputStrategy.java **/
/*****************************************************************************/

/**
 * Чтение различных представлений графа задаётся стратегиями
 */
interface InputStrategy {
    void parseString(String input, ConvertableGraph graph, int line) throws NumberFormatException, WrongInputFormatException;
}

// Для списка смежных вершин невзвешенного графа
class AdjListSimpleStrategy implements InputStrategy{
    @Override
    public void parseString(String input, ConvertableGraph graph, int line) {
        if (input.length() == 0) return;
        for (String num: input.split(" ")){
            graph.addEdge(line, Integer.valueOf(num)-1, 1);
        }
    }
}

// Для списка смежных вершин взвешенного графа
class AdjListWeightedStrategy implements InputStrategy{
    @Override
    public void parseString(String input, ConvertableGraph graph, int line) throws WrongInputFormatException {
        if (input.length() == 0) return;
        String[] parts = input.split(" ");
        if (parts.length % 2 != 0) throw new WrongInputFormatException();
        for (int i=0; i<parts.length; i+=2){
            graph.addEdge(line, Integer.valueOf(parts[i])-1, Integer.valueOf(parts[i+1]));
        }
    }
}

// Для матрицы смежности любого графа
class AdjMatrixStrategy implements InputStrategy{
    @Override
    public void parseString(String input, ConvertableGraph graph, int line) throws NumberFormatException, WrongInputFormatException {
        String[] parts = input.split(" ");
        if (parts.length != graph.getVertexCount()) throw new WrongInputFormatException();
        for (int i=0; i<parts.length; ++i){
            int weight = Integer.valueOf(parts[i]);
            if (weight != 0) graph.addEdge(line, i, weight);
        }
    }
}

// Для списка рёбер невзвешенного графа
class ListOfEdgesSimpleStrategy implements InputStrategy{
    @Override
    public void parseString(String input, ConvertableGraph graph, int line) throws NumberFormatException, WrongInputFormatException {
        String[] parts = input.split(" ");
        if (parts.length != 2) throw new WrongInputFormatException();
        graph.addEdge(Integer.valueOf(parts[0])-1, Integer.valueOf(parts[1])-1, 1);
        // todo добавить новую стратегию для неориентированного графа
        // hotfix после уточнения формата ввода-вывода
        if (!graph.isOriented())
            graph.addEdge(Integer.valueOf(parts[1])-1, Integer.valueOf(parts[0])-1, 1);
    }
}

// Для списка рёбер взвешенного графа
class ListOfEdgesWeightedStrategy implements InputStrategy{
    @Override
    public void parseString(String input, ConvertableGraph graph, int line) throws NumberFormatException, WrongInputFormatException {
        String[] parts = input.split(" ");
        if (parts.length != 3) throw new WrongInputFormatException();
        graph.addEdge(Integer.valueOf(parts[0])-1, Integer.valueOf(parts[1])-1, Integer.valueOf(parts[2]));
        // hotfix после уточнения формата ввода-вывода
        if (!graph.isOriented())
            graph.addEdge(Integer.valueOf(parts[1])-1, Integer.valueOf(parts[0])-1, Integer.valueOf(parts[2]));
    }
}

class WrongInputFormatException extends Exception{
    public WrongInputFormatException() {
        super();
    }
}
/*****************************************************************************/
/** Kruscal.java **/
/*****************************************************************************/
class Kruscal extends MSTAlgorithm {
    private PriorityQueue<Map.Entry<Edge, Integer>> edges;
    private DSU dsu;
    private ListOfEdgesGraph result;
    public Kruscal(ListOfEdgesGraph graph) {
        super(graph);
        edges = new PriorityQueue<>(graph.getVertexCount(), new Comparator<Map.Entry<Edge, Integer>>() {
            @Override
            public int compare(Map.Entry<Edge, Integer> left, Map.Entry<Edge, Integer> right) {
                if (left.getValue() < right.getValue()) return -1;
                if (left.getValue().equals(right.getValue())) return 0;
                return 1;
            }
        });
        for(Map.Entry<Edge, Integer> edge: graph.getRepresentation().entrySet()){
            edges.add(edge);
        }
        result = new ListOfEdgesGraph(graph.getVertexCount(), false, true);
        dsu = new DSU(graph.getVertexCount());
    }

    @Override
    public ConvertableGraph solve() {
        Map.Entry<Edge, Integer> temp;
        while (dsu.hasMultipleComponents() && !edges.isEmpty()){
            temp = edges.poll();
            if (dsu.inSameComponent(temp.getKey().getStart(), temp.getKey().getEnd()))
                continue;
            dsu.unite(temp.getKey().getStart(), temp.getKey().getEnd());
            result.getRepresentation().put(temp.getKey(), temp.getValue());
            result.addEdge(temp.getKey().getEnd(), temp.getKey().getStart(), temp.getValue());
        }
        return result;
    }
}
/*****************************************************************************/
/** ListOfEdgesGraph.java **/
/*****************************************************************************/
/**
 * Список смежности на основе хеш-таблицы
 */
class ListOfEdgesGraph extends ConvertableGraph{
    private HashMap<Edge, Integer> representation;
    private int edgeCounter;

    public ListOfEdgesGraph(int vertexCount, boolean oriented, boolean weighted) {
        super(vertexCount, oriented, weighted);
        setTypeLiteral('E');
        this.representation = new HashMap<>();
        edgeCounter = 0;
    }

    @Override
    public void addEdge(int from, int to, int weight) {
        super.addEdge(from, to, weight);
        representation.put(new Edge(from, to), weight);
        edgeCounter++;
    }

    @Override
    public void removeEdge(int from, int to) {
        super.removeEdge(from, to);
        representation.remove(new Edge(from, to));
        edgeCounter--;
    }

    @Override
    public int changeEdge(int from, int to, int newWeight) {
        Edge e = new Edge(from, to);
        int old = representation.get(e);
        representation.put(e, newWeight);
        return old;
    }

    @Override
    public ListOfEdgesGraph toListOfEdgesGraph() {
        return this;
    }

    @Override
    protected void copyData(GraphInterface newGraph) {
        for(Map.Entry entry: representation.entrySet()){
            Edge edge = (Edge)entry.getKey();
            newGraph.addEdge(edge.getStart(), edge.getEnd(), (Integer)entry.getValue());
        }
    }


    // TODO: вынести сравнения за цикл
    @Override
    protected void writeRepresentation(FileWriter writer) throws IOException {
        for (Map.Entry<Edge, Integer> entry: representation.entrySet()){
            // избегаем вывода дублирующихся рёбер в неориентированном графе
            if (!isOriented() && (entry.getKey().getStart() > entry.getKey().getEnd()))
                continue;
            if (isWeighted())
                writer.write(entry.getKey() + " " + entry.getValue() + "\n");
            else
                writer.write(entry.getKey() + "\n");
        }
    }

    @Override
    protected void writeHeader(FileWriter writer) throws IOException {
        if (edgeCounter != representation.size())
            edgeCounter = representation.size();
        if (isOriented()) {
            writer.write(getTypeLiteral() + " " + getVertexCount() + " " + edgeCounter + "\n");
        }
        else {
            writer.write(getTypeLiteral() + " " + getVertexCount() + " " + edgeCounter/2 + "\n");
        }
        writer.write((isOriented() ? "1" : "0") + " " + (isWeighted() ? "1": "0") + "\n");
    }

    @Override
    public HashMap<Edge, Integer> getRepresentation() {
        return representation;
    }
}


/*****************************************************************************/
/** MSTAlgorithm.java **/
/*****************************************************************************/
abstract class MSTAlgorithm {
    ConvertableGraph graph;
    public MSTAlgorithm(ConvertableGraph graph){
        this.graph = graph;
    }
    abstract ConvertableGraph solve();
}
/*****************************************************************************/
/** Prim.java **/
/*****************************************************************************/
class Prim extends MSTAlgorithm {
    private boolean[] visited;
    private PriorityQueue<Map.Entry<Integer, Map.Entry<Integer, Integer>>> outsideEdges;
    private AdjListGraph result;

    public Prim(AdjListGraph graph) {
        super(graph);
        init();
    }

    private void init(){
        visited = new boolean[this.graph.getVertexCount()];
        for(int i=0; i<this.graph.getVertexCount(); ++i){
            visited[i] = false;
        }
        outsideEdges = new PriorityQueue<>(this.graph.getVertexCount(), new Comparator<Map.Entry<Integer, Map.Entry<Integer, Integer>>>() {
            @Override
            public int compare(Map.Entry<Integer, Map.Entry<Integer, Integer>> left, Map.Entry<Integer, Map.Entry<Integer, Integer>> right) {
                if (left.getValue().getValue() < right.getValue().getValue()) return -1;
                if (left.getValue().getValue().equals(right.getValue().getValue())) return 0;
                return 1;
            }
        });
        result = new AdjListGraph(graph.getVertexCount(), false, true);
    }

    @Override
    public ConvertableGraph solve() {
        // внешний цикл нужен для построения остовного леса, если граф несвязный
        for (int i=0; i<graph.getVertexCount(); ++i){
            if (!visited[i]) {
                addNeighboursToQueue(i);
                while (!outsideEdges.isEmpty()) step();
            }
        }
        return result;
    }

    private void step(){
        Map.Entry<Integer, Map.Entry<Integer, Integer>> temp;
        temp = outsideEdges.poll();
        if (visited[temp.getKey()] == visited[temp.getValue().getKey()]){
            return;
        }
        result.addEdge(temp.getKey(), temp.getValue().getKey(), temp.getValue().getValue());
        result.addEdge(temp.getValue().getKey(), temp.getKey(), temp.getValue().getValue());
        addNeighboursToQueue(temp.getValue().getKey());
    }

    private void addNeighboursToQueue(int vertex){
        if (visited[vertex]) return;
        visited[vertex] = true;
        for (Map.Entry<Integer, Integer> entry: ((AdjListGraph)graph).getRepresentation().get(vertex).entrySet()){
            outsideEdges.add(new AbstractMap.SimpleEntry<Integer, Map.Entry<Integer, Integer>>(vertex, entry));
        }

    }
}
